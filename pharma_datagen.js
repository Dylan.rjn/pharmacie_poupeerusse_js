let main_client_list = [];
let main_medic_list = [];
let first_names = ["Roger","Michel","Emile","Louise","René","Pierre"]
let last_names = ["Mouloud","Sanchez","Giocanti","James","Ikea","Husson"]
let medic_names = ["doliprane","temesta","prozac","ketamine","viagra","betadine","smecta","vogalene"]
let medic_condition = ["20","250","3000"]

//generate client list
for(i=0;i<20;i++){
    let myname = first_names[Math.floor(Math.random()*first_names.length)] + " " + last_names[Math.floor(Math.random()*last_names.length)];
    main_client_list[i] = new Client(myname,100);
}
//generate medic list
for(j=0;j<30;j++){
    let mymedic = medic_names[Math.floor(Math.random()*medic_names.length)] + " " + medic_condition[Math.floor(Math.random()*medic_condition.length)];
    main_medic_list[j] = new Medic(mymedic,100,20);
}

let mypharma = new Pharma("lepharma","dr ledoc",main_client_list,main_medic_list)

//console.log(main_client_list)
//console.log(main_medic_list)

//mypharma.affiche()

